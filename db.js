const faker = require('faker')

module.exports = () => {
  let data = {
    taxes: [],
    tax_inputs: [],
    tax_submissions: [],
  }

  // Taxes
  const generateTax = () => ({
    id: faker.datatype.uuid(),
    name: 'Tax fake name',
    year: faker.datatype.number({
      min: 2019,
      max: 2021,
    }),
  })

  const taxFields = [
    {
      id: 'name',
      label: 'Name',
      placeholder: 'Your first name',
      type: 'text',
      maxLength: 20,
    },
    {
      id: 'surname',
      label: 'Surname',
      placeholder: 'Your last name',
      type: 'text',
      maxLength: 40,
    },
    {
      id: 'age',
      label: 'Age',
      placeholder: 'Your age',
      type: 'number',
    },
  ]

  for (let i = 0; i < 15; i++) {
    const tax = generateTax()
    data.taxes.push(tax)
    data.tax_inputs.push({ id: tax.id, fields: taxFields })
  }

  return data
}
