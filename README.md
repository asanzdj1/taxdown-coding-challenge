First step 🥇

First, you have to create a login form that allows a user to access the application.

Once logged in, the application will show a dashboard with a list of active user taxes. The information of these taxes will be obtained from a fake API built with json-server or similar. https://github.com/typicode/json-server

endpoint: /taxes

{
	"taxes":  [
		{
			"id":  "1",
			"name":  "Tax Season 2021",
			"year": "2021",
		},
		{
			"id":  "2",
			"name":  "Tax Season 2020",
			"year": "2020",
		},
	]
}
The information obtained from the API must be stored in the store using Redux, the use of middleware such as Redux Saga for handling asynchronous side effects will be a plus.

Second step 🥈

Each tax will contain a button that will allow adding submissions.

When the button is clicked, it will take you to a new screen where a request will be made to the fake API to obtain a list of inputs.

/taxes/{id}/form
{
	"inputFields":  [
		{
			"id":  "name",
			"label":  "Name",
			"placeholder":  "Your first name",
			"type":  "text",
			"maxLength":  20
		},
		{
			"id":  "surname",
			"label":  "Surname",
			"placeholder":  "Your last name",
			"type":  "text",
			"maxLength":  40
		},
		{
			"id":  "age",
			"label":  "Age",
			"placeholder":  "Your age",
			"type":  "number",
		}
	]
}
Here, a dynamic form should be rendered based on the inputs defined in the JSON response.

Remember that some fields may have validations in their keys (maxLength for the text inputs) and they can increase or change in the future.

Third step 🥉 (These medals should go from bronce to gold)

When the inputs are completed and the user clicks on submit button a POST request to /taxes/{id}/form with the values of the form will be made.

A list of submissions should be created in the store for that specific tax.

Note: It will be submissions lists for the different taxes.

All submissions by mapping them so we can see the key/value pairs introduced by the user.

Would be a screen where will be showed the taxes submissions and where the user can filter by tax year, name, surname and age

	Tax: 2021
		Submission 1
			Name: Bruce
			Surname: Wayne
			Age: 26
		Submission 2
			Name: Clark
			Surname: Kent
			Age: 22
And last but not least, it would be great if you added one or two tests to check for render stability or proper mapping of values ⭐️. We use jest and react-testing-library to testing.

Easy and simple no? So we are done! 🚀!

Optional

Add the option to edit and delete each submission.
