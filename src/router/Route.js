import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Route as ReactRoute } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Route = ({ secured, children, ...rest }) => {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated)

  if (rest.path === '/login' && isAuthenticated) {
    return (
      <Redirect
        to={{
          pathname: '/',
        }}
      />
    )
  }

  if (secured && !isAuthenticated) {
    return (
      <Redirect
        to={{
          pathname: '/login',
        }}
      />
    )
  }

  return <ReactRoute {...rest}>{children}</ReactRoute>
}

Route.propTypes = {
  children: PropTypes.object,
  currentLocation: PropTypes.string,
  isAuthenticated: PropTypes.bool,
  secured: PropTypes.bool,
}

Route.defaultProps = {
  secured: false,
  isAuthenticated: false,
}

export default Route
