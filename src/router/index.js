import React from 'react'
import { Switch } from 'react-router-dom'

import Route from './Route'
import { Home, Login, TaxDetail, NewTaxSubmission, Submissions } from '../pages'

const Router = () => {
  return (
    <div className="app__container">
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/submissions" secured>
          <Submissions />
        </Route>
        <Route path="/taxes/:id/form" secured>
          <NewTaxSubmission />
        </Route>
        <Route path="/taxes/:id" secured>
          <TaxDetail />
        </Route>
        <Route path="/" secured>
          <Home />
        </Route>
      </Switch>
    </div>
  )
}

export default Router
