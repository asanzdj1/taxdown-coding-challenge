import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { useParams, useHistory } from 'react-router-dom'
import { isEmpty, map, omit } from 'lodash'

import { Button } from '../../components'
import TaxesActions from '../../store/redux/taxes'
import { ReactComponent as BulbIcon } from '../../assets/icons/bulb.svg'

const TaxDetail = () => {
  const dispatch = useDispatch()
  const { id } = useParams()
  const tax = useSelector((state) => state.taxes.tax)
  const history = useHistory()

  useEffect(() => {
    dispatch(TaxesActions.fetchTax(id, history))
    dispatch(TaxesActions.fetchTaxSubmissions(id))
  }, [])

  const handleDelete = () => {
    dispatch(TaxesActions.deleteTax(id, history))
  }

  const navigateToForm = () => {
    history.push(`/taxes/${id}/form`)
  }

  return (
    tax && (
      <div className="tax-detail">
        <div className="tax-detail__actions">
          <Button
            className="delete-tax"
            variant="outline"
            onClick={handleDelete}
          >
            Delete
          </Button>
          <Button
            className="add-submissions"
            variant="outline"
            onClick={navigateToForm}
          >
            Add submissions
          </Button>
        </div>
        <div className="tax-detail__basic-info">
          <p className="year paragraph">{tax.year}</p>
          <p className="name heading-2">{tax.name}</p>
        </div>
        {!isEmpty(tax.submissions) ? (
          <div className="tax-detail__submissions">
            {tax.submissions.map((submission) => (
              <div key={submission.id} className="submission">
                {map(omit(submission, 'tax_id'), (value, key) => (
                  <p className="paragraph" key={key}>
                    <span className="submission__label">{key}</span>
                    <span>: {value}</span>
                  </p>
                ))}
              </div>
            ))}
          </div>
        ) : (
          <div className="tax-detail__empty-submissions">
            <BulbIcon />
            <p className="paragraph">
              Try adding some submissions for this tax
            </p>
          </div>
        )}
      </div>
    )
  )
}

TaxDetail.propTypes = {
  history: PropTypes.object,
}

export default TaxDetail
