import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { map } from 'lodash'

import { Tax } from '../../components'
import TaxesActions from '../../store/redux/taxes'

const Home = () => {
  const dispatch = useDispatch()
  const taxes = useSelector((state) => state.taxes.taxes)

  useEffect(() => {
    dispatch(TaxesActions.fetchTaxes())
  }, [])

  return (
    <div className="home">
      <h1 className="heading-1 home__title">Your active taxes</h1>
      <div className="home__taxes">
        {map(taxes, (value, key) => (
          <div key={key} className="home__taxes__tax-group">
            <h2 className="heading-2 year">{key}</h2>

            {value.map((tax) => (
              <Tax key={tax.id} id={tax.id} name={tax.name} year={tax.year} />
            ))}
          </div>
        ))}
      </div>
    </div>
  )
}

export default Home
