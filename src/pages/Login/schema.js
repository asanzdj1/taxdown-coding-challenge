import { object, string } from 'yup'

const schema = object().shape({
  email: string().email('Invalid email address').required('Required'),
  password: string().required('Required'),
})

export default schema
