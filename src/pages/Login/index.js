import React from 'react'
import { useDispatch } from 'react-redux'

import { Button, Form, TextField } from '../../components'
import schema from './schema'
import AuthActions from '../../store/redux/auth'

const Login = () => {
  const dispatch = useDispatch()

  return (
    <div className="login">
      <h1 className="heading-1">Login into your account</h1>
      <Form
        initialValues={{ email: 'example@email.com', password: 'testme' }}
        onSubmit={(values) => {
          dispatch(AuthActions.signIn({ ...values }))
        }}
        validationSchema={schema}
      >
        {({ errors, handleChange, values }) => (
          <>
            <TextField
              error={errors.email}
              label="Email"
              name="email"
              onChange={handleChange}
              type="email"
              value={values.email}
            />
            <TextField
              error={errors.password}
              label="Password"
              name="password"
              onChange={handleChange}
              type="password"
              value={values.password}
            />
            <Button className="submit" type="submit">
              Login
            </Button>
          </>
        )}
      </Form>
    </div>
  )
}

export default Login
