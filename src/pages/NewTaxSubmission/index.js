import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory, useParams } from 'react-router-dom'

import { Button, Form, TextField } from '../../components'
import TaxesActions from '../../store/redux/taxes'
import { isEmpty } from 'lodash'

const NewTax = () => {
  const { id } = useParams()
  const dispatch = useDispatch()
  const formFields = useSelector((state) => state.taxes.taxFields)
  const [initialValues, setInitialValues] = useState({})
  const history = useHistory()

  useEffect(() => {
    dispatch(TaxesActions.fetchTaxFields(id))
  }, [])

  useEffect(() => {
    setInitialValues(
      formFields.reduce((carry, field) => ({ ...carry, [field.id]: '' }), {}),
    )
  }, [formFields])

  // NOTE:
  // Temporal patch to solve formik issue with initialValues
  // Refactor: Take a look at https://final-form.org/react/
  return (
    !isEmpty(initialValues) && (
      <div className="new-tax">
        <h1 className="heading-1 new-tax__title">Tax submissions</h1>
        <Form
          initialValues={initialValues}
          onSubmit={(values) => {
            dispatch(TaxesActions.createTaxSubmissions(id, values, history))
          }}
        >
          {({ errors, handleChange, values }) => {
            return (
              <>
                {formFields.map(({ id, type, ...rest }) => {
                  return (
                    <TextField
                      key={id}
                      name={id}
                      type={type}
                      error={errors[id]}
                      onChange={handleChange}
                      value={values[id]}
                      {...rest}
                    />
                  )
                })}
                <div className="new-tax__buttons">
                  <Link to="/">
                    <Button className="cancel">Cancel</Button>
                  </Link>
                  <Button className="submit" type="submit">
                    Add
                  </Button>
                </div>
              </>
            )
          }}
        </Form>
      </div>
    )
  )
}

export default NewTax
