import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { isEmpty, map } from 'lodash'

import TaxesActions from '../../store/redux/taxes'
import { Button, TextField } from '../../components'

const Submissions = () => {
  const dispatch = useDispatch()
  const submissions = useSelector((state) => state.taxes.submissions)
  const [searchText, setSearchText] = React.useState('')
  const [range, setRange] = React.useState(['', ''])

  useEffect(() => {
    dispatch(TaxesActions.fetchSubmissions())
  }, [])

  const handleSearch = () => {
    dispatch(TaxesActions.fetchSubmissions(searchText, range[0], range[1]))
  }

  const clearFilters = () => {
    setSearchText('')
    setRange(['', ''])
    dispatch(TaxesActions.fetchSubmissions())
  }

  const handleSetMin = (e) => {
    const min = e.target.value
    const max = range[1] || parseFloat(min) + 1
    setRange([min, max])
  }
  const handleSetMax = (e) => {
    const max = e.target.value
    const min = range[0] || parseFloat(max) - 1
    setRange([min, max])
  }

  return (
    <div className="submissions">
      <div className="submissions__filters">
        <div className="range">
          <TextField
            value={range[0]}
            onChange={handleSetMin}
            placeholder="Min."
            type="number"
          />
          <TextField
            value={range[1]}
            onChange={handleSetMax}
            placeholder="Max."
            type="number"
          />
        </div>
        <TextField
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          placeholder="Type something to search..."
        />
        <div className="filter__buttons">
          <Button onClick={clearFilters} variant="outline">
            Clear
          </Button>
          <Button onClick={handleSearch}>Filter</Button>
        </div>
      </div>
      {isEmpty(submissions) && <p className="paragraph">No results found :(</p>}
      {/* eslint-disable-next-line no-unused-vars */}
      {submissions.map(({ id, tax_id, ...submission }) => (
        <div className="submissions__submission" key={id}>
          <div className="header">
            <p className="paragraph header__id">{id}</p>
          </div>
          {map(submission, (value, key) => (
            <p className="paragraph" key={key}>
              <span className="label">{key}</span>
              <span>: {value}</span>
            </p>
          ))}
        </div>
      ))}
    </div>
  )
}

export default Submissions
