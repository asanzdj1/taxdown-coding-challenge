import { createReducer, createActions } from 'reduxsauce'
import { groupBy } from 'lodash'

const initialState = {
  submissions: [],
  tax: null,
  taxFields: [],
  taxes: {},
}

const { Types, Creators } = createActions(
  {
    setSubmissions: ['submissions'],
    setTax: ['tax'],
    setTaxFields: ['fields'],
    setTaxSubmissions: ['submissions'],
    setTaxes: ['taxes'],
    // Async
    createTaxSubmissions: ['id', 'fields', 'history'],
    deleteTax: ['id', 'history'],
    fetchSubmissions: ['term', 'min', 'max'],
    fetchTax: ['id', 'history'],
    fetchTaxFields: ['id'],
    fetchTaxSubmissions: ['id'],
    fetchTaxes: null,
  },
  {
    prefix: 'TAXES_',
  },
)

const handlers = {
  [Types.SET_TAX]: (state = initialState, { tax }) => ({ ...state, tax: tax }),
  [Types.SET_TAXES]: (state = initialState, { taxes }) => ({
    ...state,
    taxes: groupBy(taxes, 'year'),
  }),
  [Types.SET_TAX_FIELDS]: (state = initialState, { fields }) => ({
    ...state,
    taxFields: fields,
  }),
  [Types.SET_SUBMISSIONS]: (state = initialState, { submissions }) => ({
    ...state,
    submissions,
  }),
  [Types.SET_TAX_SUBMISSIONS]: (state = initialState, { submissions }) => ({
    ...state,
    tax: {
      ...state.tax,
      submissions: submissions,
    },
  }),
}

export const reducer = createReducer(initialState, handlers)
export const TaxesTypes = Types
export default Creators
