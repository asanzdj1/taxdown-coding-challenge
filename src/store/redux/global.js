import { createReducer, createActions } from 'reduxsauce'

export const initialState = {
  alert: null,
}

const { Types, Creators } = createActions(
  {
    showAlert: { msg: null, alertType: 'error' },
    hideAlert: null,
  },
  {
    prefix: 'GLOBAL_',
  },
)

export const handlers = {
  [Types.HIDE_ALERT]: (state = initialState) => ({ ...state, alert: null }),
  [Types.SHOW_ALERT]: (state = initialState, { msg, alertType }) => ({
    ...state,
    alert: { msg, type: alertType },
  }),
}

export const reducer = createReducer(initialState, handlers)
export const GlobalTypes = Types
export default Creators
