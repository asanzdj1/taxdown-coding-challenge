import { createReducer, createActions } from 'reduxsauce'

export const initialState = {
  isAuthenticated: false,
  token: null,
}

const { Types, Creators } = createActions(
  {
    setToken: ['token'],
    clearSession: null,
    //Async
    signIn: ['fields'],
    signOut: null,
  },
  {
    prefix: 'AUTH_',
  },
)

const handlers = {
  [Types.SET_TOKEN]: (state = initialState, { token }) => ({
    ...state,
    token: token,
    isAuthenticated: true,
  }),
  [Types.CLEAR_SESSION]: (state = initialState) => ({
    ...state,
    token: null,
    isAuthenticated: false,
  }),
}

export const reducer = createReducer(initialState, handlers)
export const AuthTypes = Types
export default Creators
