import { combineReducers } from 'redux'

import { reducer as auth } from './redux/auth'
import { reducer as global } from './redux/global'

import { reducer as taxes } from './redux/taxes'

import rootSaga from './sagas'
import configureStore from './configureStore'

const rootReducer = combineReducers({ auth, global, taxes })

export default () => configureStore(rootReducer, rootSaga)
