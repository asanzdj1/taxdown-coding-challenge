import { call, put } from 'redux-saga/effects'

import Api from '../../Api'
import AuthActions from '../redux/auth'
import GlobalActions from '../redux/global'

export function* signIn({ fields }) {
  const { ok, data } = yield call(Api.login, fields)

  if (ok) {
    yield put(AuthActions.setToken(data.access_token))
  } else {
    yield put(
      GlobalActions.showAlert({ msg: "Email and password don't match" }),
    )
  }
}

export function* signOut() {
  const { ok } = yield call(Api.logout)

  if (ok) {
    yield put(AuthActions.clearSession())
  }
}
