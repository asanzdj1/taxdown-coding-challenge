import { call, put } from 'redux-saga/effects'

import Api from '../../Api'
import TaxesActions from '../redux/taxes'
import GlobalActions from '../redux/global'

export function* fetchTaxes() {
  const { ok, data } = yield call(Api.fetchTaxes)

  if (ok) {
    yield put(TaxesActions.setTaxes(data))
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: 'An error occurred fetching your taxes, please, retry',
      }),
    )
  }
}

export function* fetchTax({ id, history }) {
  const { ok, data } = yield call(Api.fetchTax, id)

  if (ok) {
    yield put(TaxesActions.setTax(data))
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: 'The tax you are trying to access was delete or not exist',
      }),
    )
    history.push('/')
  }
}

export function* deleteTax({ id, history }) {
  const { ok } = yield call(Api.deleteTax, id)

  if (ok) {
    yield put(
      GlobalActions.showAlert({
        msg: 'The tax was deleted successfully',
        alertType: 'success',
      }),
    )
    history.push('/')
  } else {
    yield put(
      GlobalActions.showAlert({ msg: "Uppps, we couldn't delete the tax" }),
    )
  }
}

export function* fetchTaxSubmissions({ id }) {
  const { ok, data } = yield call(Api.fetchTaxSubmissions, id)

  if (ok) {
    yield put(TaxesActions.setTaxSubmissions(data))
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: "We couldn't fetch the tax submissions, please, retry",
      }),
    )
  }
}
export function* fetchSubmissions({ term }) {
  const searchQuery = term ? `?q=${term}` : ''

  const { ok, data } = yield call(Api.fetchSubmissions, searchQuery)

  if (ok) {
    yield put(TaxesActions.setSubmissions(data))
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: "We could' fetch the data, please, retry",
      }),
    )
  }
}

export function* fetchTaxFields({ id }) {
  const { ok, data } = yield call(Api.fetchTaxFields, id)

  if (ok) {
    yield put(TaxesActions.setTaxFields(data.fields))
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: 'We could fetch the form fields, please, retry',
      }),
    )
  }
}

export function* createTaxSubmissions({ id, fields, history }) {
  const { ok } = yield call(Api.createTaxSubmissions, { ...fields, tax_id: id })

  if (ok) {
    history.push(`/taxes/${id}`)
  } else {
    yield put(
      GlobalActions.showAlert({
        msg: 'Uppsss, something weird happened creating the submissions, please, retry',
      }),
    )
  }
}
