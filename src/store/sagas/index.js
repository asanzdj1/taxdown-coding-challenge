import { all, takeLatest } from 'redux-saga/effects'

import { signIn, signOut } from './auth'
import { AuthTypes } from '../redux/auth'

import {
  createTaxSubmissions,
  deleteTax,
  fetchSubmissions,
  fetchTax,
  fetchTaxFields,
  fetchTaxSubmissions,
  fetchTaxes,
} from './taxes'
import { TaxesTypes } from '../redux/taxes'

function* rootSaga() {
  const sagas = [
    takeLatest(AuthTypes.SIGN_IN, signIn),
    takeLatest(AuthTypes.SIGN_OUT, signOut),
    takeLatest(TaxesTypes.FETCH_TAXES, fetchTaxes),
    takeLatest(TaxesTypes.FETCH_TAX, fetchTax),
    takeLatest(TaxesTypes.DELETE_TAX, deleteTax),
    takeLatest(TaxesTypes.FETCH_TAX_FIELDS, fetchTaxFields),
    takeLatest(TaxesTypes.CREATE_TAX_SUBMISSIONS, createTaxSubmissions),
    takeLatest(TaxesTypes.FETCH_TAX_SUBMISSIONS, fetchTaxSubmissions),
    takeLatest(TaxesTypes.FETCH_SUBMISSIONS, fetchSubmissions),
  ]

  yield all(sagas)
}

export default rootSaga
