import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import GlobalActions from '../store/redux/global'

const Alert = () => {
  const alert = useSelector((state) => state.global.alert)
  const dispatch = useDispatch()

  /* eslint-disable no-undef */
  useEffect(() => {
    const timer = setTimeout(() => {
      dispatch(GlobalActions.hideAlert())
    }, 3500)
    return () => clearTimeout(timer)
  }, [])
  /* eslint-enable no-undef */

  return <div className={`alert alert--${alert.type}`}>{alert.msg}</div>
}

export default Alert
