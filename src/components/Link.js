import React from 'react'
import PropTypes from 'prop-types'
import { Link as RouterLink } from 'react-router-dom'

const Link = ({ children, className, ...rest }) => (
  <RouterLink className={`link ${className}`} {...rest}>
    {children}
  </RouterLink>
)

Link.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string,
  ]),
}

export default Link
