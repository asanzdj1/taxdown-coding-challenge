import React from 'react'
import PropTypes from 'prop-types'

const TextField = ({ label, field, error, ...props }) => {
  return (
    <label className="text-field">
      <span className="text-field__label">{label}</span>
      <input
        className={`text-field__input ${
          error ? 'text-field__input--error' : ''
        }`}
        {...field}
        {...props}
      />
      <span className="text-field__error">{error}</span>
    </label>
  )
}

TextField.propTypes = {
  error: PropTypes.string,
  field: PropTypes.object,
  label: PropTypes.string,
}

export default TextField
