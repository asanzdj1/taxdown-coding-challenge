import React from 'react'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'

const Form = ({ children, initialValues, onSubmit, validationSchema }) => {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
  })

  return (
    <form className="form" onSubmit={formik.handleSubmit}>
      {children({ ...formik })}
    </form>
  )
}

Form.propTypes = {
  children: PropTypes.func,
  initialValues: PropTypes.object,
  onSubmit: PropTypes.func,
  validationSchema: PropTypes.object,
}

export default Form
