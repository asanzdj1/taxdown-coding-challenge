import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ children, className, variant, ...props }) => {
  return (
    <button className={`button ${className} button--${variant}`} {...props}>
      {children}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  className: PropTypes.string,
  variant: PropTypes.oneOf(['outline', 'solid']),
}

Button.defaultProps = {
  variant: 'solid',
}

export default Button
