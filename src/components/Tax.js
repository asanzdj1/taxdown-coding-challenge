import React from 'react'
import PropTypes from 'prop-types'

import { Link } from './index'

const Tax = ({ id, name }) => {
  return (
    <Link to={`/taxes/${id}`} className="tax">
      <p className="paragraph tax__id">{id}</p>
      <p className="paragraph tax__name">{name}</p>
    </Link>
  )
}

Tax.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
  year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

export default Tax
