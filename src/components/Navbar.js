import React from 'react'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'

import { Button, Link } from '.'
import AuthActions from '../store/redux/auth'

const links = [
  {
    id: 'home',
    label: 'Home',
    pathname: '/',
  },
  {
    id: 'submissions',
    label: 'Submissions',
    pathname: '/submissions',
  },
]

const Navbar = () => {
  const dispatch = useDispatch()
  const location = useLocation()

  return (
    <div className="navbar">
      <div className="navbar__links">
        {links.map(({ pathname, id, label }) => (
          <Link
            to={pathname}
            key={id}
            className={`${
              pathname.localeCompare(location.pathname) === 0 && 'link--active'
            }`}
          >
            {label}
          </Link>
        ))}
      </div>
      <div className="navbar__logout">
        <Button
          variant="outline"
          onClick={() => dispatch(AuthActions.signOut())}
        >
          Log out
        </Button>
      </div>
    </div>
  )
}

export default Navbar
