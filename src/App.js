import React from 'react'
import { useSelector } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import Router from './router'
import { Navbar, Alert } from './components'
import './stylesheets/base.scss'

const calculateAppHeight = () => {
  const doc = document.documentElement
  doc.style.setProperty('--app-height', `${window.innerHeight}px`)
}

window.addEventListener('resize', calculateAppHeight)
calculateAppHeight()

const App = () => {
  const alert = useSelector((state) => state.global.alert)
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated)

  return (
    <div className="app">
      <BrowserRouter>
        {!!alert && <Alert />}
        {isAuthenticated && <Navbar />}
        <Router />
      </BrowserRouter>
    </div>
  )
}

export default App
