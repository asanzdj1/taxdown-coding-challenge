import { create } from 'apisauce'

const api = create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 3000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

const isFineLoginData = (email, password) =>
  email === 'example@email.com' && password === 'testme'

const apiWithEndpoints = {
  login: ({ email, password }) =>
    isFineLoginData(email, password)
      ? {
          ok: true,
          data: {
            access_token: 'TestAccessToken',
            email,
          },
        }
      : {
          ok: false,
        },
  logout: () => ({ ok: true, data: { access_token: null } }),
  fetchTaxes: () => api.get('/taxes'),
  fetchTaxFields: (id) => api.get(`/taxes/${id}/form`),
  fetchTax: (id) => api.get(`/taxes/${id}`),
  deleteTax: (id) => api.delete(`/taxes/${id}`),
  createTaxSubmissions: (fields) => api.post('/taxes/submissions', fields),
  fetchTaxSubmissions: (id) => api.get(`/taxes/${id}/submissions`),
  fetchSubmissions: (queryParams) =>
    api.get(`/taxes/submissions${queryParams}`),
}

export default apiWithEndpoints
